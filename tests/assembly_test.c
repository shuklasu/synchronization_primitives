#include <stdio.h>
#include "types.h"
#include "assembly.h"

/*
Expected Output
------------------------------------
Test xchg8
new value = 1	old value = 255
------------------------------------
Test xchg16
new value = 1	old value = 65535
------------------------------------
Test xchg32
new value = 1	old value = 4294967295
------------------------------------
Test xchg64
new value = 1	old value = 18446744073709551615
------------------------------------
*/

int main()	{
	UINT8 a = 0xFF;
	UINT16 b = 0xFFFF;
	UINT32 c = 0xFFFFFFFF;
	UINT64 d = 0xFFFFFFFFFFFFFFFF;

	printf("------------------------------------\n");
	printf("Test xchg8\n");
	printf("new value = %u\told value = %u\n", a, xchg8(&a, 1));

	printf("------------------------------------\n");
	printf("Test xchg16\n");
	printf("new value = %u\told value = %u\n", b, xchg16(&b, 1));

	printf("------------------------------------\n");
	printf("Test xchg32\n");
	printf("new value = %u\told value = %u\n", c, xchg32(&c, 1));

	printf("------------------------------------\n");
	printf("Test xchg64\n");
	printf("new value = %lu\told value = %lu\n", d, xchg64(&d, 1));

	printf("------------------------------------\n");
	return 0;
}
