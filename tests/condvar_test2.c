#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include "types.h"
#include "synchronization.h"

#define NUM_THREADS  128
#define BLOCK 100

UINT64 end_index = BLOCK;
UINT64 completeAccesses = 0;
UINT64 numAccesses = 0;

mutex count_mutex_1;
condvar count_threshold_cv_1;
mutex count_mutex_2;
condvar count_threshold_cv_2;

void storeStats(UINT64 accessNo)	{
	mutex_lock(&count_mutex_2);
	if(completeAccesses < BLOCK)	cond_wait(&count_threshold_cv_2);
	completeAccesses = 0;
  	mutex_unlock(&count_mutex_2);

    usleep(10000);
	printf("Chunk Store Stats %lu\n", ((accessNo+1)/BLOCK));

	mutex_lock(&count_mutex_1);
	end_index = end_index + BLOCK;
	cond_broadcast(&count_threshold_cv_1);
  	mutex_unlock(&count_mutex_1);
}

void cacheAccess(UINT64 accessNo)	{
	mutex_lock(&count_mutex_1);
	while(accessNo >= end_index)	cond_wait(&count_threshold_cv_1);
  	mutex_unlock(&count_mutex_1);
	
    usleep(1000);
	printf("Cache Access %lu\n", accessNo);

	mutex_lock(&count_mutex_2);
	completeAccesses++;
	if(completeAccesses == BLOCK)	cond_signal(&count_threshold_cv_2);
  	mutex_unlock(&count_mutex_2);
}

void *threadStart(void *t) 
{
	int i;
  long my_id = (long)t;
  for(i = 0; i < 1000; i++)	{
	UINT64 accessNo = __sync_fetch_and_add(&numAccesses, 1);
	cacheAccess(accessNo);
	if((accessNo+1)%BLOCK == 0)	storeStats(accessNo);
  }
  pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
  int rc; 
  pthread_t threads[NUM_THREADS];
  long t[NUM_THREADS];
  pthread_attr_t attr;

  /* Initialize mutex and condition variable objects */
  mutex_init(&count_mutex_1);
  mutex_init(&count_mutex_2);
  cond_init (&count_threshold_cv_1, &count_mutex_1);
  cond_init (&count_threshold_cv_2, &count_mutex_2);

  /* For portability, explicitly create threads in a joinable state */
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
  UINT32 i;
  for(i = 0; i < NUM_THREADS; i++)	{
  	t[i] = i;
	pthread_create(&threads[i], &attr, threadStart, (void *)t[i]);
  }

  /* Wait for all threads to complete */
  for (i = 0; i < NUM_THREADS; i++) {
    pthread_join(threads[i], NULL);
  }
  printf ("Main(): Waited on %u threads. Done.\n", NUM_THREADS);

  /* Clean up and exit */
  pthread_attr_destroy(&attr);
  mutex_destroy(&count_mutex_1);
  cond_destroy(&count_threshold_cv_1);
  mutex_destroy(&count_mutex_1);
  cond_destroy(&count_threshold_cv_2);
  pthread_exit (NULL);

}


