#ifndef __TYPES__
#define __TYPES__

typedef unsigned char UINT8;
typedef unsigned short UINT16;
typedef unsigned int UINT32;
typedef unsigned long UINT64;

#endif
