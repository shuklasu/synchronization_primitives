#ifndef __ASSEMBLY__
#define __ASSEMBLY__

#include "types.h"

static inline UINT8 xchg8(UINT8 *ptr, UINT8 x)	{
	__asm__ __volatile__("xchgb %0, %1"
							: "=r" (x)
							: "m"(*ptr), "0" (x)
							: "memory");
	return x;
}

static inline UINT16 xchg16(UINT16 *ptr, UINT16 x)	{
	__asm__ __volatile__("xchgw %0,%1"
							:"=r" (x)
							:"m" (*ptr), "0" (x)
							:"memory");
	return x;
}

static inline UINT32 xchg32(UINT32 *ptr, UINT32 x)	{
	__asm__ __volatile__("xchgl %0,%1"
							:"=r" (x)
							:"m" (*ptr), "0" (x)
							:"memory");
	return x;
}

static inline UINT64 xchg64(UINT64 *ptr, UINT64 x)	{
	__asm__ __volatile__("xchgq %0,%1"
							:"=r" (x)
							:"m" (*ptr), "0" (x)
							:"memory");
	return x;
}

#endif
